from PySide import QtGui, QtCore
import os
import sys

class FileRenamer(QtGui.QWidget):

    def __init__(self):
        super(FileRenamer, self).__init__()
        self.main()

    def main(self):
        self.setWindowTitle('FileRenamer')
        self.main_layout = QtGui.QVBoxLayout()
        self.main_layout.setContentsMargins(2, 2, 2, 2)
        self.lineLabel = QtGui.QLabel('Season prefix:')
        self.userPrefixEntry = QtGui.QLineEdit('')
        self.showLabel = QtGui.QLabel('Show Title(Make sure to select in order):')
        self.showEntry = QtGui.QLineEdit('')

        self.lineLabel.setAlignment(QtCore.Qt.AlignLeft)
        self.showLabel.setAlignment(QtCore.Qt.AlignLeft)

        self.main_layout.addWidget(self.lineLabel)
        self.main_layout.addWidget(self.userPrefixEntry)
        self.main_layout.addWidget(self.showLabel)
        self.main_layout.addWidget(self.showEntry)
        self.openDialog = QtGui.QPushButton('Get files to rename')
        self.openDialog.released.connect(lambda: self.requestFiles())
        self.fileList = QtGui.QListWidget()

        self.renameButton = QtGui.QPushButton('Rename')
        self.renameButton.released.connect(lambda: self.rename())

        self.main_layout.addWidget(self.openDialog)
        self.main_layout.addWidget(self.fileList)
        self.main_layout.addWidget(self.renameButton)
        self.setLayout(self.main_layout)
        self.show()

    def rename(self):
        filesToRename= []
        if self.userPrefixEntry.text() and self.showEntry.text():

            userSeasonNumber = self.userPrefixEntry.text()
            userShowIndex = 1

            rowCount = self.fileList.count()
            for row in range(0, rowCount):
                filesToRename.append(self.fileList.item(row).text())

            for f in filesToRename:
                fileName = os.path.basename(f)
                pathDir = os.path.dirname(f)
                fileExt = os.path.splitext(f)
                if userShowIndex < 10:
                    userShowIndex = str(0) + str(userShowIndex)
                    os.rename(f, os.path.join(pathDir, userSeasonNumber + userShowIndex + '_' + self.showEntry.text() + fileExt[1]))
                    userShowIndex.replace('0', '')
                    userShowIndex = int(userShowIndex)
                    userShowIndex += 1
                else:
                    os.rename(f, os.path.join(pathDir, userSeasonNumber + str(userShowIndex) + '_' + self.showEntry.text()) + fileExt[1])
                    userShowIndex = int(userShowIndex)
                    userShowIndex += 1

    def requestFiles(self):
        try:
            fDialog = QtGui.QFileDialog()
            fDialog.setFileMode(QtGui.QFileDialog.ExistingFiles)
            fDialog.setViewMode(QtGui.QFileDialog.Detail)

            if fDialog.exec_():
                fileNames = fDialog.selectedFiles()
        except:
            return

        allRows = self.fileList.count()
        for row in range(0, allRows):
            self.fileList.clear()

        if fileNames:
            for f in fileNames:
                fListItem = QtGui.QListWidgetItem(f)
                self.fileList.addItem(fListItem)


app = QtGui.QApplication([])
ex = FileRenamer()
sys.exit(app.exec_())
